package test.randomizer.java.com.cache;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;

import org.junit.jupiter.params.provider.MethodSource;
import randomizer.java.com.cache.RepeatingNumbers;

import java.util.ArrayList;
import java.util.Arrays;


public class RepeatingNumbersTest {
    RepeatingNumbers cut = new RepeatingNumbers();

    static Arguments[] checkRepeatingNumberTestArgs() {
        return new Arguments[]{
                Arguments.arguments(3, true),
                Arguments.arguments(7, true),
                Arguments.arguments(8, false),
                Arguments.arguments(2, false)

        };
    }

    @ParameterizedTest
    @MethodSource("checkRepeatingNumberTestArgs")
    void checkRepeatingNumberTest(int number, boolean expected){
        cut.addRepeatingNumber(3);
        cut.addRepeatingNumber(6);
        cut.addRepeatingNumber(7);
        boolean actual = cut.checkRepeatingNumber(number);
        Assertions.assertEquals(expected,actual);
        cut.clearCache();
    }


    @Test
    void clearCacheTest(){
        cut.addRepeatingNumber(3);
        cut.addRepeatingNumber(6);
        cut.addRepeatingNumber(7);
        cut.clearCache();
        ArrayList<Integer> expected = new ArrayList<>();
        ArrayList<Integer> actual = cut.getRepeatingNumbers();
        Assertions.assertEquals(expected,actual);
    }


    static Arguments[] addRepeatingNumberTestArgs() {
        return new Arguments[]{
                Arguments.arguments(1, 2, new ArrayList<Integer>(Arrays.asList(1, 2))),
                Arguments.arguments(3, 4, new ArrayList<Integer>(Arrays.asList(3, 4))),

        };
    }

    @ParameterizedTest
    @MethodSource("addRepeatingNumberTestArgs")
    void addRepeatingNumberTest(int number, int number2, ArrayList<Integer> expected){
        cut.addRepeatingNumber(number);
        cut.addRepeatingNumber(number2);
        ArrayList<Integer> actual = cut.getRepeatingNumbers();
        Assertions.assertEquals(expected,actual);
        cut.clearCache();
    }




}
