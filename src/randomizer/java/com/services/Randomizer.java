package randomizer.java.com.services;

import randomizer.java.com.cache.RepeatingNumbers;

import java.util.Random;
import java.util.Scanner;

public class Randomizer {
    private int max;
    private int min;
    private int indexOfRepeating;
    private int rangeOfRepeating;
    RepeatingNumbers repeatingNumbers = new RepeatingNumbers();
    Scanner in = new Scanner(System.in);
    Random rnd = new Random();
    public void getMinAndMax(){
        indexOfRepeating=0;
        rangeOfRepeating=0;
        System.out.println("Welcome to RANDOMIZER \n To start, set the range ьшт фтв ьфч from 1 to 500");
        System.out.println("min = ");
        int userMin = in.nextInt();;
        System.out.println("max = ");
        int userMax = in.nextInt();;
        if (userMin>=1 && userMax>userMin && userMax<=500){
            min = userMin;
            max = userMax;
            rangeOfRepeating = max-min+1;
            getCommand();
        }
        else{
            System.out.println("You entered incorrect values, try again");
            getMinAndMax();;
        }
    }
    private void getCommand(){
        System.out.println("Enter command or enter help to find out more");
        String command  = in.next();
        switch (command) {
            case  ("generate"):
                generate();
                break;
            case ("help"):
                help();
                break;
            case ("exit"):
                exit();
                break;
            default:
                System.out.println("You entered an invalid command, try again");
                getCommand();
                break;
        }
    }

    private void generate(){
        int number = min + rnd.nextInt(max - min + 1);
        if (indexOfRepeating<rangeOfRepeating)
        {
            if(repeatingNumbers.checkRepeatingNumber(number)){
                generate();
            }
            else
            {
                indexOfRepeating++;
                System.out.println(number);
                repeatingNumbers.addRepeatingNumber(number);
                getCommand();
            }
        }
        else
        {
            System.out.println("All numbers have already been generated, try to set the range again");
            getMinAndMax();
        }
    }
    private void help()
    {
        System.out.println("Enter the command generate to generate a number in the given range");
        System.out.println("Enter the command exit to exit the application");
        System.out.println(" ");
        getCommand();
    }
    private void exit()
    {
        repeatingNumbers.clearCache();
        getMinAndMax();;
    }


}
