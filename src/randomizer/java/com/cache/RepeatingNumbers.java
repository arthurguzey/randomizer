package randomizer.java.com.cache;

import java.util.ArrayList;

public class RepeatingNumbers {

     ArrayList<Integer> repeatingNumbers = new ArrayList<>();


    public ArrayList<Integer> getRepeatingNumbers() {
        return repeatingNumbers;
    }

    public void addRepeatingNumber(int number)
    {
        repeatingNumbers.add(number);
    }
    public void clearCache(){
        repeatingNumbers.clear();
    }
    public boolean checkRepeatingNumber(int number)
    {
        boolean numRepeat;
        if(repeatingNumbers.contains(number))
        {
            numRepeat = true;
        }
        else {
            numRepeat = false;
        }
        return numRepeat;
    }
    // check
    // add repeating numbers
    //clear cache
}
